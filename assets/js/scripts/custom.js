;(function ($) {
    $(document).ready(function () {
        // Homepage fullwidth slider
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            items: 1,
            dots: true,
            // autoplay: true,
            autoplayTimeout: 3000,
        });

        $('.slide-next').click(function () {
            owl.trigger('next.owl.carousel', [500]);
        });

        $('.slide-prev').click(function () {
            owl.trigger('prev.owl.carousel', [500]);
        });

        $('.owl-dot').click(function () {
            owl.trigger('to.owl.carousel', [$(this).index(), 500]);
        });

        // Clients carousel
        $('.slick-slider').slick({
            centerMode: true,
            slidesToShow: 6,
            centerPadding: '0px',
            slidesToScroll: 1,
            nextArrow: '<div class="next-slide"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
            prevArrow: '<div class="prev-slide"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        // Progress circles
        $(".progress-circle").each(function () {
            var $bar = $(this).find(".bar");
            var $val = $(this).find(".skill-value");
            var perc = parseInt($val.text(), 10);

            $({p: 0}).animate({p: perc}, {
                duration: 3000,
                easing: "swing",
                step: function (p) {
                    $bar.css({
                        transform: "rotate(" + (45 + (p * 1.8)) + "deg)", // 100%=180° so: ° = % * 1.8
                        // 45 is to add the needed rotation to have the green borders at the bottom
                    });
                    $val.text(p | 0);
                }
            });
        });

    });
})(jQuery);