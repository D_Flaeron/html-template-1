var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

gulp.task('sass', function () {
    return gulp.src('assets/sass/**/*.+(scss|sass)')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(postcss([autoprefixer({browsers: ['ie >= 10', 'last 4 versions', '> 1%']})]))
        .pipe(gulp.dest('assets/css'))
});

gulp.task('scripts', function () {
    return gulp.src('assets/js/scripts/*.js')
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'))
})

// gulp.task('css-libs', ['sass'], function () {
//     return gulp.src('assets/css/libs.css')
//         .pipe(cssnano())
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest('assets/css/libs'))
// })

gulp.task('css-minify', function () {
    return gulp.src('assets/css/main.css')
        .pipe(cssnano())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('assets/css'))
})

gulp.task('watch', function () {
    gulp.watch('assets/sass/**/*.+(scss|sass)', ['sass']);
    // gulp.watch('assets/sass/*.sass', ['css-libs']);
    gulp.watch('assets/css/main.css', ['css-minify']);
    gulp.watch('assets/js/scripts/*.js', ['scripts']);
})

gulp.task('default', ['watch']);